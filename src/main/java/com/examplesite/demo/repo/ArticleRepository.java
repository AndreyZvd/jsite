package com.examplesite.demo.repo;

import com.examplesite.demo.models.Article;
import org.springframework.data.repository.CrudRepository;

public interface ArticleRepository extends CrudRepository<Article, Long> {
}
