package com.examplesite.demo.controllers;

import com.examplesite.demo.models.Article;
import com.examplesite.demo.repo.ArticleRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class BlogController {

    @Autowired
    private ArticleRepository articleRepository;

    @GetMapping("/blog")
    public String blogMain(Model model) {
        Iterable<Article> articles = articleRepository.findAll();
        model.addAttribute("articles", articles);
        return "blog-template";
    }

    @GetMapping("/blog/add")
    public String blogAdd(Model model) {
        Iterable<Article> articles = articleRepository.findAll();
        model.addAttribute("articles", articles);
        return "add";
    }

    @PostMapping("/blog/add")
    public String blogAddOne(@RequestParam String title, @RequestParam String anons, @RequestParam String fulltext, Model model) {
        Article article = new Article(title, anons, fulltext);
        articleRepository.save(article);
        return "redirect:/blog";
    }
}
